<?php
namespace app\vues;

use web\html\Accueil;

class VueAccueil {

    private $html, $liste, $app, $grp;

    public function __construct($liste, $grp=null){
        $this->liste = $liste;
        $this->app = \Slim\Slim::getInstance();
        if ($grp != null){
        	$this->grp = $grp;
        }
        
    }

    private function afficherComptes(){
        $fragment = '<h2>Comptes</h2>';
        foreach($this->liste as $compte){
            $url = $this->app->urlFor('identification', ['id' => $compte->id]);
            $fragment .= '<div class="compte">
							<a href="' . $url . '">Nom : ' . $compte->nom . '</a>
							<p>Message : '. $compte->message . '</p>
						</div>';
        }

        $this->html .= $fragment;
    }

    private function listerUtilisateur(){
        $fragment = '<h2>Utilisateur</h2>';
        foreach($this->liste as $compte){
            $img = 'src/web/img/user/' . $compte->id.".png";
            $url = $this->app->urlFor('afficherUser', ['id' => $compte->id]);
            $fragment .= '<div class="compte">
							<p>' . $compte->nom . '</p>
							<img class="img" src="' . $img . '"/>	
							<a href="' . $url . '">Plus de details>>>></a>
						</div>';

        }

        $this->html .= $fragment;
    }
    private function afficherUtilisateur(){
        $utilisateur =$this->liste;
        $fragment = '<h2>'.$utilisateur->nom.'</h2>';
        $img = '../src/web/img/user/' . $utilisateur->id.".png";
        $fragment .= '<div class="Logement">
							<div class="image"><img class="imgAF" src="' . $img . '"/></div>
							<div class="descr">
							<h2>Message : '. $utilisateur->message . '</h2>';
        
        if (isset($_SESSION['groupe'])){
        	$url = $this->app->urlFor('ajouterUser', ['id' => $utilisateur->id]);
        	$fragment .= '<a href="' . $url . '">Ajouter cet utilisateur a mon groupe</a>';
        }
        $fragment .= '</div></div>';
        $this->html .= $fragment;
    }

    private function listerLogement(){
        $fragment = '<h2>Logement</h2>';
        foreach($this->liste as $logement){
            $img = 'src/web/img/apart/' . $logement->id.".jpg";
            $url = $this->app->urlFor('afficherLogement', ['id' => $logement->id]);
            $fragment .= '<div class="compte">
							<img class="imgL" src="' . $img . '"/>
						    <p>Nombre de place : ' . $logement->places . '</p>
							<a href="' . $url . '">Plus de details>>>></a>
						</div>';
        }

        $this->html .= $fragment;
    }
    private function afficherLogement(){
        $logement = $this->liste;
        $fragment = '<h2>Logement numero'. $logement->id.'</h2>';
        $img = '../src/web/img/apart/' . $logement->id.".jpg";
        
        $fragment .= '<div class="Logement">
                            <div class="image"><img class="imgAF" src="' . $img . '"/></div>
                           <div class="descr">
                            <h2>Nombre de place : ' . $logement->places . '</h2>';


        if (isset($_SESSION['groupe'])){
        	$url = $this->app->urlFor('ajouterLogement', ['id' => $logement->id ]);
        	$fragment .= '<a href="' . $url . '">Choisir ce logement</a>';
        }
							
        $fragment .= '</div></div>';
        $this->html .= $fragment;
    }


    public function render($selecteur){
        $groupe="";
        $urlA=$this->app->urlFor('accueil');
        $urlU=$this->app->urlFor('listerUser');
        $urlL=$this->app->urlFor('listerLogement');
        $urlG=$this->app->urlFor('mesgroupes');
        if (isset($_SESSION['profil'])){
            $groupe=<<<END
            <li class="linav"><a href='$urlG'>GROUPE</a></li>
END;
        }

        $url=URL;
        $this->html=$this->html.<<<END
                <!DOCTYPE html><html>
                       <head>
                           <meta charset="utf-8">
                           <title>Un toit pour tous</title>
                           <link href=" $url/src/web/css/Accueil.css" rel="stylesheet" type="text/css" media="all" />
                       </head>
                       <header><a href='$urlA'><img  src="$url/src/web/img/cssimg/logo.png" alt="logo"></a></header>
                       <nav>
                           <ul id="menu">
                               <li class="linav"><a href='$urlA'>HOME</a></li>
                               <li class="linav"><a href='$urlU'>USER</a>
                           <ul>
                           </ul></li>
                               <li class="linav"><a href='$urlL'>LOGEMENT</a></li>
                               $groupe
                               <li class="linav"><a href='#'>CONNEXION</a></li>
                           </ul>
                       </nav>
                       <div>
                       </div>
                       <body>
                           <div class='bodywrap'>
END;

        switch($selecteur){
            case 0:
                $this->afficherComptes();
                break;
            case 1:
                $this->listerUtilisateur();
                break;
            case 2:
                $this->afficherUtilisateur();
                break;
            case 3:
                $this->listerLogement();
                break;
            case 4:
                $this->afficherLogement();
                break;
        }

        $this->html = $this->html.<<<END
        </div>
        <footer>
        <div class="liste">
            <div>
                <h1>Un toit pour tous</h1> 
                <ul>
                     <li class="liFooter"><a href='#'>Qui nous somme</a></li>
                     <li class="liFooter"><a href='#'>Nos missions</a></li>
                </ul>
            </div>
            <div>     
                <h1>Nous contacter</h1> 
                <ul>
                     <li class="liFooter"><a href='#'>Contact@unToitPourTous.fr</a></li>
                     <li class="liFooter"><a href='#'>tel : 03 29 ** ** **</a></li>
                </ul>
            </div>
            <img  src="$url/src/web/img/cssimg/logo.png" alt="logo"></a></header>
         </div>
        </footer>
        </body></html>
END;

        echo $this->html;
    }
}