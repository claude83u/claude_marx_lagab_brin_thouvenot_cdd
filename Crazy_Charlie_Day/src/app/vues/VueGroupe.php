<?php
namespace app\vues;


use app\modeles\Groupe;

class VueGroupe {

	private $html, $groupes, $groupeP, $app;

	public function __construct($groupes, $groupeP=null){
		$this->groupes = $groupes;
		$this->groupeP = $groupeP;
		$this->app = \Slim\Slim::getInstance();
	}

	private function afficherGroupes(){
		$fragment = '<h2>Groupes auxquels j\'appartient</h2>';
		foreach($this->groupes as $groupe){
			$url = $this->app->urlFor('identification', ['id' => $groupe->id]);
			$fragment .= '<div class="compte">
							<a href="' . $url . '">Groupe : ' . $groupe->id_groupe . '</a>
							<p>Description : '. $groupe->description . '</p>
						</div>';
		}

		$this->html .= $fragment;
	}
	
	private function afficherGroupeP(){
		$fragment = '<h2>Groupe que j\'ai cree</h2>';
		$url = $this->app->urlFor('monGroupe');
		$fragment .= '<div class="compte">
						<a href="' . $url . '"> Detail du groupe </a>
						<p>Description : '. $this->groupeP['description'] . '</p>
					</div>';
	
		$this->html .= $fragment;
	}
	
	private function afficherCreation(){
		$url = $this->app->urlFor('creerGroupe');
		$fragment = '<form action="' . $url . '" method="post"
						<label>Description du groupe : </label><input type="text" name="description">
						<input type="submit" value="Creer groupe">
					 </form>';
		$this->html .= $fragment;
	}
	
	private function afficherGroupe(){
		$fragment = '<h1>Mon groupe</h1>';
		$logement = null;
		if (isset($this->groupes['logement'])){
			$logement = $this->groupes['logement'];
		}
		
		if ($logement != null){
			$img = 'src/web/img/apart/' . $logement->id.".jpg";
			$url = $this->app->urlFor('afficherLogement', ['id' => $logement->id]);
			$fragment .= '<h3>Logement</h3>
					<div class="Logment">
						<img class="imgL" src="' . $img . '"/>
						<p>Nombre de place : ' . $logement->places . '</p>
						<a href="' . $url . '">Plus de details>>>></a>
					  </div>';
		}
		
		$membres = [];
		if (isset($this->groupes['membres'])){
			$membres = $this->groupes['membres'];
		}
		$fragment .= '<h3>Personnes invitees<h3>';
		foreach($membres as $membre){
			$img = 'src/web/img/user/' . $membre->id.".jpg";
			$url = $this->app->urlFor('afficherUser', ['id' => $membre->id]);
			$fragment .= '<div class="compte">
							<p>Nom : ' . $membre->nom . '</p>
							<img class="img" src="' . $img . '"/>
							<a href="' . $url . '">Plus de details>>>></a>
						</div>';
		}
		$this->html .= $fragment;
	}
	
	public function render($selecteur){

		$urlA=$this->app->urlFor('accueil');
		$urlU=$this->app->urlFor('listerUser');
		$urlL=$this->app->urlFor('listerLogement');
		$urlG=$this->app->urlFor('mesgroupes');

		$url=URL;
		$this->html=$this->html.<<<END
                <!DOCTYPE html><html>
                       <head>
                           <meta charset="utf-8">
                           <title>Un toit pour tous</title>
                           <link href=" $url/src/web/css/Accueil.css" rel="stylesheet" type="text/css" media="all" />
                       </head>
                       <header><a href='$urlA'><img  src="$url/src/web/img/cssimg/logo.png" alt="logo"></a></header>
                       <nav>
                           <ul id="menu">
                               <li class="linav"><a href='$urlA'>HOME</a></li>
                               <li class="linav"><a href='$urlU'>USER</a>
                           <ul>
                           </ul></li>
                               <li class="linav"><a href='$urlL'>LOGEMENT</a></li>
                               <li class="linav"><a href='$urlG'>GROUPE</a></li>
                               <li class="linav"><a href='#'>CONNEXION</a></li>
                           </ul>
                       </nav>
                       <div>
                       </div>
                       <body>
                           <div class='bodywrap'>
END;
		switch($selecteur){
			case 0:
				if ($this->groupeP != null){
					$this->afficherGroupeP();
				} else {
					$this->afficherCreation();
				}
				if (sizeof($this->groupes) > 0){
					$this->afficherGroupes();
				}
				break;
			case 1:
				$this->afficherGroupe();
				break;
		}
		
		$this->html = $this->html.<<<END
        </div></body></html>
END;

		echo $this->html;
	}
}