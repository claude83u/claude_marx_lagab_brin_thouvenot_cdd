<?php

/**
 * Created by PhpStorm.
* User: quent
* Date: 09/02/2017
* Time: 10:18
*/
namespace app\modeles;

class Groupe extends \Illuminate\Database\Eloquent\Model {
	protected $table = 'groupe';
	protected $primaryKey = 'id_groupe';
	public $timestamps = false;
	
	function membres(){
		return $this->belongsToMany('app\modeles\User', 'groupe_user', 'id_groupe', 'id_personne');
	}

	function proprietaire(){
        return $this->belongsTo('app\modeles\User','id_proprietaire');
    }

    function logement(){
        return $this->belongsTo('app\modeles\Logement','id_logement');
    }
}
