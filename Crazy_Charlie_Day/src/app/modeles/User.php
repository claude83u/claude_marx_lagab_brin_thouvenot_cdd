<?php

namespace app\modeles;
class User extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'user';
    protected $primaryKey = 'id';
    public $timestamps = false;
    
    public function groupes() {
    	return $this->belongsToMany('app\modeles\Groupe', 'groupe_user', 'id_personne', 'id_groupe');
    }
    
}