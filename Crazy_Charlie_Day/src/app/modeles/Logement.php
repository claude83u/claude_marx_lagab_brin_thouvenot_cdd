<?php

/**
 * Created by PhpStorm.
 * User: quent
 * Date: 09/02/2017
 * Time: 10:18
 */
namespace app\modeles;

class Logement extends \Illuminate\Database\Eloquent\Model {
    protected $table = 'logement';
    protected $primaryKey = 'id';
    public $timestamps = false;
}