<?php
namespace app\controleurs;

use app\vues\VueAccueil;
use app\modeles\User;
use Slim\Slim;

class ControlAccueil{

	function accueil(){
        $comptes = User::all();

        $v = new VueAccueil($comptes);
        $v->render(0);
    }
	
    function identification($id){
        if (isset($_SESSION['groupe']))
            unset($_SESSION['groupe']);
    	$_SESSION['profil'] = $id;
    	$app = \Slim\Slim::getInstance();
    	$app->redirect($app->urlFor('listerUser'));

    }
    

    
    
}