<?php
/**
 * Created by PhpStorm.
 * User: quent
 * Date: 09/02/2017
 * Time: 14:26
 */

namespace app\controleurs;


use app\modeles\User;
use app\modeles\Groupe;
use app\vues\VueGroupe;
use app\modeles\Logement;

class ControlGroupe{


    function afficherGroupes(){
        if (!isset($_SESSION['profil'])){
            $app = \Slim\Slim::getInstance();
            $app->redirect($app->urlFor('accueil'));
        }
        $idUser = $_SESSION['profil'];
        $user = User::find($idUser);
        if ($user != null){
            $groupes = $user->groupes;
            if (isset($_SESSION['groupe'])) {
                $groupeProp = $_SESSION['groupe'];
                $v = new VueGroupe($groupes,$groupeProp);
            }
            else{
                $v = new VueGroupe($groupes);
            }

            $v->render(0);

        }

    }

    function creerGroupe(){

        $app = \Slim\Slim::getInstance();
        if (!isset($_SESSION['profil'])){

            $app->redirect($app->urlFor('accueil'));
        }
        $idUser = $_SESSION['profil'];
        $user = User::find($idUser);


        if ($user != null){

            if(isset($_SESSION['groupe'])){
                var_dump($_SESSION['groupe']);
            }
            else {

                $groupe = Groupe::where('id_proprietaire',$idUser)->first();
                if ($groupe == null) {
                    if (isset($_POST['description'])) {
                        $desc = $app->request->post('description');
                        echo 'blabla2';
                        $desc = filter_var($desc, FILTER_SANITIZE_STRING);
                        echo 'blabla1';
                        if (!empty($desc)) {
                            $_SESSION['groupe'] = [];
                            $_SESSION['groupe']['description'] = $desc;
                            $_SESSION['groupe']['membres'] = [];
                            echo 'blabla';
                            $app->redirect($app->urlFor('mesgroupes'));


                        }
                    }
                }
            }
        }

    }

    function afficherGroupe(){
        if (!isset($_SESSION['profil']) || !isset($_SESSION['groupe'])){
            $app = \Slim\Slim::getInstance();
            $app->redirect($app->urlFor('accueil'));
        } else {
        	$idUser = $_SESSION['profil'];
        	$groupe = $_SESSION['groupe'];
        	$groupe2 = [];
        	if (isset($_SESSION['groupe']['logement'])){
        		$groupe2['logement'] = Logement::find($_SESSION['groupe']['logement']);
        	}
        	if (isset($_SESSION['groupe']['membres'])){
        		$groupe2['membres'] = User::whereIn('id', $_SESSION['groupe']['membres'])->get();
        	}
        	$groupe2['description'] = $groupe['description'];
        	$v = new VueGroupe($groupe2);
        	$v->render(1);
        }
    }
    
    function ajouterUser($id){

    	if (!isset($_SESSION['profil'])){
    		$app = \Slim\Slim::getInstance();
    		$app->redirect($app->urlFor('listerUser'));
    	}
    	$idUser = $_SESSION['profil'];
    	 
    	$user = User::find($id);

        $estDansGroupe = array_search($id,$_SESSION['groupe']['membres']);

    	if (isset($_SESSION['groupe']) && ($user != null)  && ($id != $idUser) && !$estDansGroupe){
//    		$groupe->membres()->attach($id);
//    		$groupe->save();
            $_SESSION['groupe']['membres'][] = $id;
    	}
    	$app = \Slim\Slim::getInstance();
    	$app->redirect($app->urlFor('listerUser'));
    	
    }



    function ajouterLogement($id){
    	if (!isset($_SESSION['profil'])) {
            $app = \Slim\Slim::getInstance();
            $app->redirect($app->urlFor('listerLogement'));
        }
    
    	$logement = Logement::find($id);


    	if (isset($_SESSION['groupe']) && ($logement != null) ){


            $_SESSION['groupe']['logement'] = $id;
//    		$groupe->id_logement = $id;
//    		$groupe->save();
    	}
    	$app = \Slim\Slim::getInstance();
    	$app->redirect($app->urlFor('listerLogement'));
    	 
    }


//    function validerGroupe(){
//
//        $app = \Slim\Slim::getInstance();
//        if (!isset($_SESSION['profil'])){
//
//            $app->redirect($app->urlFor('listerLogement'));
//        }
//        $groupe = Groupe::where('id_proprietaire',$_SESSION['profil'])->first();
//        if ($groupe != null){
//            $groupe->Etat = 'valider';
//            $groupe->save();
//        }
//        $app->redirect($app->urlFor('accueil'));
//
//    }
//
//    function genererUrl(){
//
//        $app = \Slim\Slim::getInstance();
//        uniqid("url",true);
//
//
//    }




}