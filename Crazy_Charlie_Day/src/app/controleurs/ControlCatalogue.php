<?php
namespace app\controleurs;

use app\vues\VueAccueil;
use app\modeles\User;
use app\modeles\Logement;
use app\modeles\Groupe;
class ControlCatalogue{

    private $app;

    function __contruct()
    {
        $this->app = \Slim\Slim::getInstance();
    }
    function listerUser(){

        //retounrne tous les noms des utilisateurs
        $listeUtilisateur = User::all();

        $vue = new VueAccueil($listeUtilisateur);
        $vue->render(1);

    }
    function afficherUser($id){
    	if (!isset($_SESSION['profil'])){
    		$app = \Slim\Slim::getInstance();
    		$app->redirect($app->urlFor('accueil'));
    	}
    	$idUser = $_SESSION['profil'];
    	$groupe = Groupe::where('id_proprietaire', $idUser)->first();
    	
        $Utilisateur = User::select()->where("id","=",$id)->first();

        $vue = new VueAccueil($Utilisateur, $groupe);
        $vue->render(2);
    }
    function listerLogement(){
    	if (!isset($_SESSION['profil'])){
    		$app = \Slim\Slim::getInstance();
    		$app->redirect($app->urlFor('accueil'));
    	}
    	$idUser = $_SESSION['profil'];
    	$groupe = Groupe::where('id_proprietaire', $idUser)->first();
        //retounrne tous les noms des utilisateurs
        $listeLogement = Logement::all();

        $vue = new VueAccueil($listeLogement, $groupe);
        $vue->render(3);

    }

    function afficherLogement($id){
    	if (!isset($_SESSION['profil'])){
    		$app = \Slim\Slim::getInstance();
    		$app->redirect($app->urlFor('accueil'));
    	}
    	$idUser = $_SESSION['profil'];
    	$groupe = Groupe::where('id_proprietaire', $idUser)->first();
        $logement = Logement::select()->where("id","=",$id)->first();

        $vue = new VueAccueil($logement, $groupe);
        $vue->render(4);
    }
}