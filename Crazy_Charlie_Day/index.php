<?php

require_once 'vendor/autoload.php';
use app\controleurs\ControlAccueil;
use app\controleurs\ControlCatalogue;
use app\controleurs\ControlGroupe;


const URL = 'http://localhost:8080/claude_marx_lagab_brin_thouvenot_cdd/Crazy_Charlie_Day';

session_start();

$app = new \Slim\Slim();

try {
	$tab = parse_ini_file('src/conf/conf.ini');
	$db = new \Illuminate\Database\Capsule\Manager();
	$db->addConnection($tab);
	$db->setAsGlobal();
	$db->bootEloquent();
} catch (PDOException $e) {
	echo $e;
}
// Affichage des pages de la bibliotheque perso

$app->get('/',function(){(new ControlAccueil())->accueil();})->name('accueil');

$app->get('/Identification/:id',function($id){(new ControlAccueil())->identification($id);})->name('identification');

$app->get('/user',function(){(new ControlCatalogue())->listerUser();})->name('listerUser');

$app->get('/user/:id',function($id){(new ControlCatalogue())->afficherUser($id);})->name('afficherUser');

$app->get('/logement',function(){(new ControlCatalogue())->listerLogement();})->name('listerLogement');

$app->get('/logement/:id',function($id){(new ControlCatalogue())->afficherLogement($id);})->name('afficherLogement');

$app->get('/MesGroupes',function(){(new ControlGroupe())->afficherGroupes();})->name('mesgroupes');

$app->post('/creerGroupe',function(){(new ControlGroupe())->creerGroupe();})->name('creerGroupe');

$app->get('/MonGroupe',function(){(new ControlGroupe())->afficherGroupe();})->name('monGroupe');

$app->get('/validerGroupe',function(){(new ControlGroupe())->validerGroupe();})->name('validerGroupe');

$app->get('/AjouterUser/:id',function($id){(new ControlGroupe())->ajouterUser($id);})->name('ajouterUser');

$app->get('/AjouterLogement/:id',function($id){(new ControlGroupe())->ajouterLogement($id);})->name('ajouterLogement');

/*$app->get('/validerGroupe',function(){(new ControleurGroupe())->validerGroupe();})->name('validerGroupe');


$app->get('/inviter/;id',function($id){(new ControleurGroupe())->inviter($id);})->name('inviter');

$app->get('/refuserInvitation',function(){(new ControleurGroupe())->refuserInvitation();})->name('refuserInvitation');

$app->get('/accepterInvitation',function(){(new ControleurGroupe())->accepterInvitation();})->name('accepterInvitation');



$app->get('/listerGroupe',function(){(new ControleurBackoffice())->listerGroupe();})->name('listerGroupe');

$app->get('/validerGroupe/:id',function(){(new ControleurBackoffice())->validerGroupe();})->name('validerGroupe');

$app->get('/refuserGroupe/:id',function(){(new ControleurBackoffice())->refuserGroupe();})->name('refuserGroupe');
*/
$app->run();
?>